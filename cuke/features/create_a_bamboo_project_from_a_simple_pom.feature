@javascript
Feature: Setup Feed Notifications

    Background:
        Given I am on "http://localhost:6990/bamboo"
        Given I am logged in as "admin" with password "admin"
        Given The JDK and Maven are setup

    Scenario: Create a project from a simple pom.
        Given I am on "http://localhost:6990/bamboo/start.action"
        When I follow "Create A Plan"
        Then I should see "Create a Poject from a Maven POM"

    Scenario: Import the a pom from the Plugin Screen
        Given I am on "http://localhost:6990/bamboo/plugins/sevlet/autoplan"
        When I upload a simple pom

    
