package com.adaptavist.bamboo.autoplan;

public class TestGroupImpl implements TestGroup {
    String id;
    ProductIds productIds =  new ProductIds();
    Includes includes = new Includes();

    public TestGroupImpl(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public ProductIds getProductIds() {
        return productIds;
    }

    @Override
    public Includes getIncludes() {
        return includes;
    }


    public void addInclude(String include) {
        includes.add(include);
    }

    public void addProductId(String productId) {
        productIds.add(productId);
    }
}
