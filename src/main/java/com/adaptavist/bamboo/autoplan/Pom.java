package com.adaptavist.bamboo.autoplan;

interface Pom {

    String getScmLocation();
    String getProjectName();
    String getDescription();

    TestGroups getTestGroups();


}
