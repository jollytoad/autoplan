package com.adaptavist.bamboo.autoplan;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Includes implements Collection<String> {
    Set<String> values = new HashSet<String>();

    @Override
    public int size() {
        return values.size();
    }

    @Override
    public boolean isEmpty() {
        return values.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return values.contains(o);
    }

    @Override
    public Iterator<String> iterator() {
        return values.iterator();
    }

    @Override
    public Object[] toArray() {
        return values.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return values.toArray(ts);
    }

    @Override
    public boolean add(String s) {
        return values.add(s);
    }

    @Override
    public boolean remove(Object o) {
        return values.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return values.containsAll(objects);
    }

    @Override
    public boolean addAll(Collection<? extends String> strings) {
        return values.addAll(strings);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return values.retainAll(objects);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return values.removeAll(objects);
    }

    @Override
    public void clear() {
        values.clear();
    }

    @Override
    public boolean equals(Object o) {
        return values.equals(o);
    }

    @Override
    public int hashCode() {
        return values.hashCode();
    }
}
