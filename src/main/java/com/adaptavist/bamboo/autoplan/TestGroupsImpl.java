package com.adaptavist.bamboo.autoplan;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class TestGroupsImpl implements TestGroups {
    private final Set<TestGroup> testGroups=new HashSet<TestGroup>();

    @Override
    public Iterator<TestGroup> iterator() {
        return testGroups.iterator();
    }

    @Override
    public Object[] toArray() {
        return testGroups.toArray();
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return testGroups.toArray(ts);
    }

    @Override
    public boolean add(TestGroup testGroup) {
        return testGroups.add(testGroup);
    }

    @Override
    public boolean remove(Object o) {
        return testGroups.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> objects) {
        return testGroups.containsAll(objects);
    }

    @Override
    public boolean addAll(Collection<? extends TestGroup> testGroups) {
        return this.testGroups.addAll(testGroups);
    }

    @Override
    public boolean retainAll(Collection<?> objects) {
        return testGroups.retainAll(objects);
    }

    @Override
    public boolean removeAll(Collection<?> objects) {
        return testGroups.removeAll(objects);
    }

    public void addTestGroup(TestGroup testGroup){
        testGroups.add(testGroup);
    }

    @Override
    public int size() {
        return testGroups.size();
    }

    @Override
    public boolean isEmpty() {
        return testGroups.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return testGroups.contains(o);
    }

    @Override
    public void clear() {
        testGroups.clear();
    }
}
