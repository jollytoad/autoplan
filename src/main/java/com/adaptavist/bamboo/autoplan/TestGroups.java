package com.adaptavist.bamboo.autoplan;

import java.util.Collection;
import java.util.Iterator;

interface TestGroups extends Iterable<TestGroup>, Collection<TestGroup> {
    @Override
    public Iterator<TestGroup> iterator();
}
