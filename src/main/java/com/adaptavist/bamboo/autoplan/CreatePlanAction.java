package com.adaptavist.bamboo.autoplan;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.build.creation.JobCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService;
import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.PollingBuildStrategy;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.fieldvalue.BuildDefinitionConverter;
import com.atlassian.bamboo.fieldvalue.TaskConfigurationUtils;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.v2.build.trigger.DependencyBlockingStrategy;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.bamboo.ww2.actions.admin.ImportMavenPlanCreatePlanAction;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.spring.container.ContainerManager;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;
import org.apache.commons.configuration.XMLConfiguration;
import org.apache.commons.io.FileUtils;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.List;
import java.util.Map;

public class CreatePlanAction extends ImportMavenPlanCreatePlanAction {
    private static final String PROJECT_KEY = "projectKey";
    private static final String PROJECT_NAME = "projectName";
    private static final String NEW_PROJECT_MARKER = "newProject";
    private static final String CHAIN_KEY = "chainKey";
    private static final String CHAIN_NAME = "chainName";
    private static final String TMP_CREATE_AS_ENABLED_PROPERTY = "tmp.createAsEnabled";
    private static final String KEY_MAVEN2_TASK = "com.atlassian.bamboo.plugins.maven:task.builder.mvn2";
    private static final String MAVEN_CFG_GOAL = "goal";
    private static final long TASK_ID = 1;
    private String fullPlanKey;


    // for some reason unknown to me the bamboo action doesn't get fully injected unless explicitly autowire it from the ContainerManager
    public CreatePlanAction() {
        ContainerManager.autowireComponent(this);
    }

    @Override
    public String doExecute() throws Exception {

        final PlanCreationService.EnablePlan enablePlan;
        if (getBuildConfiguration().getBoolean(TMP_CREATE_AS_ENABLED_PROPERTY, false)) {
            enablePlan = PlanCreationService.EnablePlan.ENABLED;
        } else {
            enablePlan = PlanCreationService.EnablePlan.DISABLED;
        }

        BuildDefinitionConverter buildDefinitionConverter = (BuildDefinitionConverter) ContainerManager.getComponent("buildDefinitionConverter");

        // create the chain
        BuildConfiguration chainBuildConfiguration = new BuildConfiguration(getBuildConfiguration().asXml());
        final BuildStrategy polling = new PollingBuildStrategy();
        buildDefinitionConverter.setBuildStrategyToConfig(chainBuildConfiguration.getProjectConfig(), polling);
        buildConfiguration.setProperty(DependencyBlockingStrategy.DEPENDENCY_BLOCKING_STRATEGY_CONFIG_KEY, DependencyBlockingStrategy.BuildParentIfChangesDetected.getValue());

        ActionParametersMapImpl actionParametersMap = new ActionParametersMapImpl(ActionContext.getContext());
        PlanCreationService chainCreationService = (PlanCreationService) ContainerManager.getComponent("chainCreationService");

        String chainKey = chainCreationService.createPlan(chainBuildConfiguration, actionParametersMap, enablePlan);

        Pom pom = null;
        HttpSession session = ServletActionContext.getRequest().getSession(true);
        try {
            session.getAttribute("pomfilename");
            pom = new PomImpl((String)session.getAttribute("pomfilename"));
        } finally {
            File tempDirectory = new File((String)session.getAttribute("pomtmpdirectory"));
            FileUtils.deleteDirectory(tempDirectory);
        }


        fullPlanKey = createNewStageWithJob(chainKey, "Package", "PACKAGE", "Package Stage", "clean package", JobCreationService.NEW_STAGE_MARKER);
        boolean first = true;
        for (TestGroup group : pom.getTestGroups()) {
            final String id = group.getId().replaceAll("[\\W]", "");
            createNewStageWithJob(chainKey, "Integration Test " + group.getId(), "INTTEST" + id, "Integration Test Stage", "clean verify -DtestGroups=" + group.getId(), first ? JobCreationService.NEW_STAGE_MARKER : "Integration Test Stage");
            first = false;
        }
        createNewStageWithJob(chainKey, "Deploy", "Deploy", "Deploy Stage", "clean deploy", JobCreationService.NEW_STAGE_MARKER);


        // add notifications to chain.
        if (!getNotificationSet().getNotificationRules().isEmpty()) {
            Chain chain = planManager.getPlanByKey(chainKey, Chain.class);
            if (chain == null) {
                throw new IllegalStateException("We just created a plan but can't find it in the database");
            }

            for (NotificationRule rule : getNotificationSet().getNotificationRules()) {
                chain.getNotificationSet().addNotification(rule);
            }
            planManager.savePlan(chain);
        }
        return SUCCESS;
    }

    private String createNewStageWithJob(String newPlanKey, final String buildName, final String buildKey, final String stageName, final String mavenGoals, final String existingStage) throws PlanCreationDeniedException {
        final JobCreationService jobCreationService = (JobCreationService) ContainerManager.getComponent("jobCreationService");
        Map<String, Object> jobContext = Maps.newHashMap();
        jobContext.put(JobCreationService.BUILD_KEY, newPlanKey);
        jobContext.put(JobCreationService.BUILD_NAME, buildName);
        jobContext.put(JobCreationService.SUB_BUILD_KEY, buildKey);
        jobContext.put(JobCreationService.STAGE_NAME, stageName);
        jobContext.put(JobCreationService.EXISTING_STAGE, existingStage);
        BuildConfiguration jobBuildConfiguration = new BuildConfiguration();
        jobBuildConfiguration.setProperty(BuildDefinitionConverter.INHERIT_REPOSITORY, "true");
        addMaven2TaskToConfiguration(jobBuildConfiguration, mavenGoals, "Build for " + buildName);
        String fullPlanKey = jobCreationService.createPlan(jobBuildConfiguration, new ActionParametersMapImpl(jobContext), PlanCreationService.EnablePlan.ENABLED);
        jobCreationService.triggerCreationCompleteEvents(PlanKeys.getPlanKey(fullPlanKey));
        Job job = planManager.getPlanByKey(fullPlanKey, Job.class);
        if (job != null) {
            List<TaskDefinition> tasks = job.getBuildDefinition().getTaskDefinitions();
            for (TaskDefinition task : tasks) {
                taskManager.calculateRequirementsForTaskDefinition(job, task);
                buildDefinitionManager.savePlanAndDefinition(job, job.getBuildDefinition());
            }
        }
        return fullPlanKey;
    }

    private void addMaven2TaskToConfiguration(BuildConfiguration buildConfiguration, final String goals, final String userDescription) {
        TaskDefinition maven2Task = new TaskDefinitionImpl(TASK_ID,
                KEY_MAVEN2_TASK,
                userDescription,
                Maps.newHashMap(ImmutableMap.of("environmentVariables", "DISPLAY:=99")));

        maven2Task.getConfiguration().put(TaskConfigConstants.CFG_BUILDER_LABEL, findBestMaven2Builder());
        maven2Task.getConfiguration().put(TaskConfigConstants.CFG_JDK_LABEL, uiConfigBean.getDefaultJdkLabel());
        maven2Task.getConfiguration().put(MAVEN_CFG_GOAL, goals);
        maven2Task.getConfiguration().put(TaskConfigConstants.CFG_HAS_TESTS, Boolean.TRUE.toString());
        maven2Task.getConfiguration().put(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN_OPTION, "customTestDirectory");
        maven2Task.getConfiguration().put(TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN, "**/target/**/surefire-reports/*.xml");

        // Actually set all the values
        final XMLConfiguration config = buildConfiguration.getProjectConfig();
        TaskConfigurationUtils.addTaskDefinitionsToConfig(Lists.newArrayList(maven2Task), config, TaskConfigurationUtils.TASK_PREFIX);
    }

    /**
     * @return the name of the Maven 2 builder
     */
    private String findBestMaven2Builder() {
        return "Maven 2";
    }

    public String getFullPlanKey() {
        return fullPlanKey;
    }
}
