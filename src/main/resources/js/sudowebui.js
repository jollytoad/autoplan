
var html = "<li><a id='importMavenPlan' href='/bamboo/build/admin/create/autoplan.action'><strong>Import an Atlassian Plugin pom.xml</strong><span>You can import a Plan into Bamboo from an Atlassian Plugin Maven 2 project by getting Bamboo to parse the Plan information from this project's pom.xml file.</span></a></li>"

AJS.toInit(function($) {
    $('#creationOption').append(html);
});
